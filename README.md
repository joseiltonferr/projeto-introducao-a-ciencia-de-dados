# Análise das Informações de Acidentes ao Longo das Rodovias

Projeto da disciplina Introdução à Ciência de Dados - Universidade Federal da Paraíba

## Integrantes

* Joseilton Ferreira

## Dataset

Informações de acidentes ao longo da rodovia, obtidos nos dados abertos da ANTT - Agência Nacional de Transportes Terrestes, disponível em https://dados.antt.gov/dataset/acidentes-rodovias
